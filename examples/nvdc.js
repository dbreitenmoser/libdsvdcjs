const { libdsvdc, libdsutil } = require('../libdsvdcjs') 

const vdc = new libdsvdc({debug:false})
const vdcName= 'ioBroker Controller'
const vdcDSUID = 'A4BA33B589B1587CC0AFACA87CE7D89D02';

const devices = [
{
  dSUID: "A4BA33B589B1587CC0AFACA87CE7D89D03",
  primaryGroup: 8,
  name: 'FHEM',
  vendorName: 'KYUKA',
  configURL: 'http://192.168.1.249:18081',
  sensorDescriptions: [
    {
  	sensorName: "Aussentemperatur",
	aliveSignInterval: 360,
	dsIndex: 0,
	max: 100,
	maxPushInterval: 3300,
	min: -50,
	name: 'FHEM Aussentemperatur',
	resolution: 1,
	sensorType: 1,
	sensorUsage: 2,
	siunit: 'celsius',
	symbol: '°C',
	updateInterval: '30'
    }
  ]
}
];

vdc.start({
  vdcName: vdcName,
  vdcDSUID: vdcDSUID,
  port: 40000,
  configURL: 'http://192.168.1.249:18081'
}, devices);

vdc.on('messageReceived', msg => {
  console.log('MSG RECEIVED', JSON.stringify(msg));
})

vdc.on('messageSent', msg => {
  console.log('MSG SENT', JSON.stringify(msg));
})

setTimeout(function() {
  console.log('sending update -----------\n\n\n -------------\n');
  vdc.sendUpdate('A4BA33B589B1587CC0AFACA87CE7D89D03', [
{"name":"sensorStates","elements":[{"name":"Aussentemperatur","elements":[{"name":"age","value":{"vDouble":0.000684}},{"name":"error","value":{"vUint64":"0"}},{"name":"value","value":{"vDouble":10}}]}]}
])
},60 * 1000);
