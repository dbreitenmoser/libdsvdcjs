const { EventEmitter } = require("events");

class MyEventEmitter extends EventEmitter {
  emitObject(event, obj = {}) {
    this.emit(event, obj);
    return obj;
  }
}

module.exports = {
  MyEventEmitter,
};
