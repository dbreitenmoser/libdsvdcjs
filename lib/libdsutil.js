/**
 * Helper class with useful function to operate a VDC
 */
const libdsutil = class {
  /**
   * Main constructor
   */
  constructor() {
    this.fp = require("find-free-port");
  }

  /**
   * Generates a 34 byte dSUID
   * @returns dSUID
   */
  getDSUID() {
    const genRanHex = (size) =>
      [...Array(size)]
        .map(() => Math.floor(Math.random() * 16).toString(16))
        .join("");
    return genRanHex(34);
  }

  /**
   * Searches for free ports between 40000 and 50000 to use as the port for the VDC
   * @async
   * @returns port
   */
  async getFreePort() {
    const self = this;
    const getFP = async function () {
      let result = await Promise.resolve(
        self
          .fp(40000, 50000)
          .then(([freep]) => {
            return freep;
          })
          .catch((err) => {
            console.error(err);
            return err;
          })
      );
      return result;
    };
    let port = await getFP();
    console.log(port);
    return port;
  }
};

module.exports = libdsutil;
