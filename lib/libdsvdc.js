const protobuf = require("protobufjs");
const dnssd = require("dnssd");
const net = require("net");
const path = require("path");
const dsutil = require("./libdsutil");
// const { EventEmitter } = require("events");
const {
  binaryInputDescription,
  sensorDescription,
  outputDescription,
  outputSetting,
  channelDescription,
  createSubElements,
} = require("./messageMapping");
const { MyEventEmitter } = require("./MyEventEmitter");
// const { decode } = require("punycode");

const DSUTIL = new dsutil();
const messagePath = path.join(__dirname, "/messages/messages.proto");
const root = protobuf.loadSync(messagePath);
const vdsm = root.lookupType("Message");
const VERSION = "0.0.1";
const MODEL = "ioBroker VDC";
const VENDORNAME = "KYUKA";

/**
 * Main class to start and control a VDC
 * @param {Object} config
 */
let libdsvdc = class extends MyEventEmitter {
  /**
   * Main constructor
   * @param {Object} config
   */
  constructor(config) {
    super(config);
    this.debug = false;
    this.debug = config.debug;
    this.config = {};
    this.messageId = 0;
  }

  /**
   * Digitalstrom requires a 2 byte header with the lenght. This functions adds that header to the buffer
   * @param  {} buffer
   */
  _addHeaders(buffer) {
    function decimalToHex(d, padding) {
      var hex = Number(d).toString(16);
      padding =
        typeof padding === "undefined" || padding === null
          ? (padding = 2)
          : padding;

      while (hex.length < padding) {
        hex = "0" + hex;
      }

      return hex;
    }
    const h = decimalToHex(buffer.length, 4);
    const cA = [Buffer.from(h, "hex"), buffer];
    return Buffer.concat(cA);
  }

  /**
   * Sends a vdsmResponseHello message
   * @param  {} conn
   * @param  {} decodedMessage
   */
  _vdsmResponseHello(conn, decodedMessage) {
    const answerObj = vdsm.fromObject({
      type: 3,
      messageId: decodedMessage.messageId,
      vdcResponseHello: { dSUID: this.config.vdcDSUID },
    });
    const answerBuf = vdsm.encode(answerObj).finish();
    if (this.debug) console.log(JSON.stringify(vdsm.decode(answerBuf)));
    conn.write(this._addHeaders(answerBuf));
    /**
     * @event messageSent - New message sent to VDSM
     * @type {object}
     * @property Message Object
     */
    this.emitObject("messageSent", vdsm.decode(answerBuf));
  }

  /**
   * Sends a vdcSendPong message
   * @param  {} conn
   * @param  {} decodedMessage
   */
  _vdcSendPong(conn, decodedMessage) {
    let answerObj = vdsm.fromObject({
      type: 9,
      messageId: decodedMessage.messageId + 1,
      vdcSendPong: { dSUID: decodedMessage.vdsmSendPing.dSUID },
    });
    const answerBuf = vdsm.encode(answerObj).finish();
    if (this.debug) console.log(JSON.stringify(vdsm.decode(answerBuf)));
    conn.write(this._addHeaders(answerBuf));
    /**
     * @event messageSent - New message sent to VDSM
     * @type {object}
     * @property Message Object
     */
    this.emitObject("messageSent", vdsm.decode(answerBuf));
  }

  /**
   * Sends a vdcSendAnnounceVdc message
   * @param  {} conn
   */
  _vdcSendAnnounceVdc(conn) {
    this.messageId = this.messageId + 1;
    let answerObj = vdsm.fromObject({
      type: 23,
      messageId: this.messageId,
      vdcSendAnnounceVdc: { dSUID: this.config.vdcDSUID },
    });
    const answerBuf = vdsm.encode(answerObj).finish();
    if (this.debug) console.log(JSON.stringify(vdsm.decode(answerBuf)));
    conn.write(this._addHeaders(answerBuf));
    /**
     * @event messageSent - New message sent to VDSM
     * @type {object}
     * @property Message Object
     */
    this.emitObject("messageSent", vdsm.decode(answerBuf));
  }

  /**
   * Parses the vdsmGetProperty message to add values wherever it's known
   * @param  {} conn
   * @param  {Object} decodedMessage
   */
  _vdcResponseGetProperty(conn, decodedMessage) {
    const properties = [];
    let sendIt = true;
    if (
      decodedMessage.vdsmRequestGetProperty.dSUID.toLowerCase() ===
      this.config.vdcDSUID.toLowerCase()
    ) {
      // this is our VDC -> no lookup for devices
      decodedMessage.vdsmRequestGetProperty.query.forEach((p) => {
        if (this.debug) console.log("Query", p);
        if (p.name == "name") {
          properties.push({
            name: "name",
            value: { vString: this.config.vdcName },
          });
        } else if (p.name == "capabilities") {
          properties.push({
            name: "capabilities",
            elements: [
              {
                name: "dynamicDefinitions",
                value: { vBool: false },
              },
              {
                name: "identification",
                value: { vBool: true },
              },
              {
                name: "metering",
                value: { vBool: false },
              },
            ],
          });
          // start timer to announce devices ...
          setTimeout(() => {
            this.emit("vdcAnnounceDevices");
          }, 10 * 1000);
        } else if (p.name == "modelVersion") {
          properties.push({
            name: "modelVersion",
            value: { vString: VERSION },
          });
        } else if (p.name == "configURL" && this.config.configURL) {
          properties.push({
            name: "configURL",
            value: { vString: this.config.configURL },
          });
        } else if (p.name == "hardwareVersion") {
          properties.push({
            name: "hardwareVersion",
            value: { vString: VERSION },
          });
        } else if (p.name == "model") {
          properties.push({
            name: "model",
            value: { vString: MODEL },
          });
        } else if (p.name == "displayId") {
          properties.push({
            name: "displayId",
            value: { vString: this.config.vdcName },
          });
        } else if (p.name == "vendorName") {
          properties.push({
            name: "vendorName",
            value: { vString: VENDORNAME },
          });
        }
      });
    } else {
      // lookup device array for properties
      const device = this.devices.find(
        (d) =>
          d.dSUID.toLowerCase() ==
          decodedMessage.vdsmRequestGetProperty.dSUID.toLowerCase()
      );
      if (device) {
        decodedMessage.vdsmRequestGetProperty.query.forEach((p) => {
          if (this.debug) console.log("Query", p);
          if (p.name == "outputSettings") {
            // outputSettings
            let elements = [];
            if (device.outputSetting) {
              device.outputSetting.forEach((desc) => {
                // loop all keys of an object
                elements = [];

                for (const [key, value] of Object.entries(desc)) {
                  if (key && outputSetting.find((o) => o.name == key)) {
                    if (
                      outputSetting.find((o) => o.name == key).type ==
                      "elements"
                    ) {
                      const subElements = [];
                      value.forEach((s) => {
                        subElements.push({
                          name: s,
                          value: {
                            vBool: "true",
                          },
                        });
                      });
                      elements.push({
                        name: key,
                        elements: subElements,
                      });
                    } else {
                      const valObj = {};
                      valObj[outputSetting.find((o) => o.name == key).type] =
                        value;
                      elements.push({
                        name: key,
                        value: valObj,
                      });
                    }
                    if (this.debug) {
                      console.log("ADDED ELEMENTS", JSON.stringify(elements));
                    }
                  }
                }
              });
            }
            if (elements.length > 0) {
              properties.push({
                name: "outputSettings",
                elements: elements,
              });
            } else {
              // commented, because p44 does not send it
              /* properties.push({
                name: p.name,
                elements: [{ name: "" }],
              }); */
            }
            if (this.debug) console.log(JSON.stringify(properties));
          } else if (p.name == "outputDescription") {
            // outputDescription
            let elements = [];
            if (device.outputDescription) {
              device.outputDescription.forEach((desc) => {
                // loop all keys of an object
                elements = [];

                for (const [key, value] of Object.entries(desc)) {
                  if (key && outputDescription.find((o) => o.name == key)) {
                    const valObj = {};
                    valObj[outputDescription.find((o) => o.name == key).type] =
                      value;
                    elements.push({
                      name: key,
                      value: valObj,
                    });
                    if (this.debug) {
                      console.log("ADDED ELEMENTS", JSON.stringify(elements));
                    }
                  }
                }
              });
            }
            if (elements.length > 0) {
              properties.push({
                name: "outputDescription",
                elements: elements,
              });
            } else {
              // commented, because p44 does not send it
              /* properties.push({
                name: p.name,
                elements: [{ name: "" }],
              }); */
            }
          } else if (p.name == "buttonInputSettings") {
            // buttonInputSettings
            if (Array.isArray(device.buttonInputSetting)) {
              const biElements = [];
              device.buttonInputSetting.forEach((cdObj, i) => {
                if (
                  cdObj &&
                  typeof cdObj === "object" &&
                  !Array.isArray(cdObj) &&
                  cdObj !== null
                ) {
                  const subElements = createSubElements(cdObj);

                  biElements.push({
                    // name: `generic_${i}`,
                    name: `button`,
                    elements: subElements,
                  });
                } else {
                  properties.push({
                    // name: `generic_${i}`,
                    name: `button`,
                  });
                }
              });
              properties.push({
                name: p.name,
                elements: biElements,
              });
            } else {
              properties.push({
                name: p.name,
              });
            }
          } else if (p.name == "buttonInputDescriptions") {
            // buttonInputSettings
            if (Array.isArray(device.buttonInputDescription)) {
              const biElements = [];
              device.buttonInputDescription.forEach((cdObj, i) => {
                if (
                  cdObj &&
                  typeof cdObj === "object" &&
                  !Array.isArray(cdObj) &&
                  cdObj !== null
                ) {
                  const subElements = createSubElements(cdObj);

                  biElements.push({
                    // name: `generic_${i}`,
                    name: `button`,
                    elements: subElements,
                  });
                } else {
                  properties.push({
                    // name: `generic_${i}`,
                    name: `button`,
                  });
                }
              });
              properties.push({
                name: p.name,
                elements: biElements,
              });
            } else {
              // commented, because p44 does not send it
              /* properties.push({
                name: p.name,
                elements: [{ name: "" }],
              }); */
            }
          } else if (p.name == "sensorDescriptions") {
            // sensorDescriptions
            let elements = [];
            const sensorElements = [];
            if (device.sensorDescription) {
              // console.log("SENSOR DESCRIPTIONS", device.sensorDescription);
              device.sensorDescription.forEach((desc) => {
                // console.log("PROCESSING OBJECT", JSON.stringify(desc));
                // loop all keys of an object
                elements = [];

                for (const [key, value] of Object.entries(desc)) {
                  if (key && sensorDescription.find((o) => o.name == key)) {
                    const valObj = {};
                    valObj[sensorDescription.find((o) => o.name == key).type] =
                      value;
                    elements.push({
                      name: key,
                      value: valObj,
                    });
                    if (this.debug) {
                      console.log(
                        "ADDED ELEMENTS SENSOR",
                        JSON.stringify(elements)
                      );
                    }
                  }
                }
                sensorElements.push({
                  name: desc.sensorName,
                  elements: elements,
                });
              });
            }
            /* console.log(
              "\n\n\n\nELEMENTS OF SENSORDESCRIPTIONS\n\n\n",
              JSON.stringify(elements),
              elements.length
            ); */
            if (elements.length > 0) {
              properties.push({
                name: "sensorDescriptions",
                elements: sensorElements,
              });
            } else {
              properties.push({
                name: "sensorDescriptions",
              });
            }
          } else if (p.name == "zoneID") {
            properties.push({
              name: p.name,
              value: {
                vUint64: device.zoneID || 65534,
              },
            });
          } else if (p.name == "binaryInputDescriptions") {
            // binaryInputDescriptions
            if (Array.isArray(device.binaryInputDescription)) {
              const biElements = [];
              device.binaryInputDescription.forEach((cdObj, i) => {
                if (
                  cdObj &&
                  typeof cdObj === "object" &&
                  !Array.isArray(cdObj) &&
                  cdObj !== null
                ) {
                  const subElements = createSubElements(cdObj);

                  biElements.push({
                    // name: `generic_${i}`,
                    // name: `generic`,
                    name: cdObj.name,
                    elements: subElements,
                  });
                } else {
                  properties.push({
                    // name: `generic_${i}`,
                    name: `generic`,
                  });
                }
              });
              properties.push({
                name: p.name,
                elements: biElements,
              });
            } else {
              properties.push({
                name: p.name,
                elements: [{ name: "" }],
              });
            }
          } else if (p.name == "binaryInputSettings") {
            // binaryInputDescriptions
            if (Array.isArray(device.binaryInputSetting)) {
              const biElements = [];
              device.binaryInputSetting.forEach((cdObj, i) => {
                if (
                  cdObj &&
                  typeof cdObj === "object" &&
                  !Array.isArray(cdObj) &&
                  cdObj !== null
                ) {
                  const subElements = createSubElements(cdObj);

                  biElements.push({
                    // name: `generic_${i}`,
                    // name: `generic`,
                    name: cdObj.inputName,
                    elements: subElements,
                  });
                } else {
                  properties.push({
                    // name: `generic_${i}`,
                    name: `generic`,
                  });
                }
              });
              properties.push({
                name: p.name,
                elements: biElements,
              });
            } else {
              properties.push({
                name: p.name,
              });
            }
          } else if (p.name == "sensorSettings") {
            // binaryInputDescriptions
            if (Array.isArray(device.sensorSetting)) {
              const biElements = [];
              device.sensorSetting.forEach((cdObj, i) => {
                if (
                  cdObj &&
                  typeof cdObj === "object" &&
                  !Array.isArray(cdObj) &&
                  cdObj !== null
                ) {
                  const subElements = createSubElements(cdObj);

                  biElements.push({
                    // name: `generic_${i}`,
                    // name: `generic`,
                    name: cdObj.sensorName,
                    elements: subElements,
                  });
                } else {
                  properties.push({
                    // name: `generic_${i}`,
                    name: `generic`,
                  });
                }
              });
              properties.push({
                name: p.name,
                elements: biElements,
              });
            } else {
              properties.push({
                name: p.name,
              });
            }
          } else if (p.name == "deviceActionDescriptions") {
            // deviceActionDescriptions
            properties.push({
              name: p.name,
            });
          } else if (p.name == "customActions") {
            // customActions
            properties.push({
              name: p.name,
            });
          } else if (p.name == "dynamicActionDescriptions") {
            // dynamicActionDescriptions
            properties.push({
              name: p.name,
            });
          } else if (p.name == "deviceStates") {
            // deviceStates
            properties.push({
              name: p.name,
            });
          } else if (p.name == "deviceProperties") {
            // deviceStates
            properties.push({
              name: p.name,
            });
          } else if (p.name == "channelDescriptions") {
            // channelDescriptions
            if (Array.isArray(device.channelDescription)) {
              device.channelDescription.forEach((cdObj) => {
                if (
                  cdObj &&
                  typeof cdObj === "object" &&
                  !Array.isArray(cdObj) &&
                  cdObj !== null
                ) {
                  const subElements = createSubElements(cdObj);

                  properties.push({
                    name: p.name,
                    elements: subElements,
                  });
                } else {
                  properties.push({
                    name: p.name,
                  });
                }
              });
            } else {
              properties.push({
                name: p.name,
              });
            }
          } else if (p.name == "deviceIcon16") {
            // deviceIcon16
            properties.push({
              name: "deviceIcon16",
              value: {
                vBytes:
                  "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAJZlWElmTU0AKgAAAAgABQESAAMAAAABAAEAAAEaAAUAAAABAAAASgEbAAUAAAABAAAAUgExAAIAAAARAAAAWodpAAQAAAABAAAAbAAAAAAAAAAaAAAAAQAAABoAAAABd3d3Lmlua3NjYXBlLm9yZwAAAAOgAQADAAAAAQABAACgAgAEAAAAAQAAABCgAwAEAAAAAQAAABAAAAAAlOnVuQAAAAlwSFlzAAAEAAAABAABGSOaawAAAi1pVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDYuMC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyI+CiAgICAgICAgIDx4bXA6Q3JlYXRvclRvb2w+d3d3Lmlua3NjYXBlLm9yZzwveG1wOkNyZWF0b3JUb29sPgogICAgICAgICA8dGlmZjpZUmVzb2x1dGlvbj4yNjwvdGlmZjpZUmVzb2x1dGlvbj4KICAgICAgICAgPHRpZmY6T3JpZW50YXRpb24+MTwvdGlmZjpPcmllbnRhdGlvbj4KICAgICAgICAgPHRpZmY6WFJlc29sdXRpb24+MjY8L3RpZmY6WFJlc29sdXRpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgpHDvotAAADJElEQVQ4EU1TXWhcVRCeOefu3b272eaP3bWxaKs2NlEUqX1I0QfZtSUISrWLYiKEUHxr8UHz44sLIklarG0kIUJBNAqtAS2ipqWxSYkRn/QtNE2Tgj+NSRbSZHdz9+bee44zew3NPJw7d2a+OfPNnEFgyWYljI35rD54/KN0cfZOZvexI/9U7dv/IYCObMzdPJmfnHnCTOy6/O943w2O28ZIyOUEDA8rCsRE2j6PwhzSSj1nNSQXI4nUAQAMOct355214rsojY7Y3sMPlBZ/+RFmZzUnkTA1pRmczPSMy8iuNlA+aK98ydr78IhZUzdHvqly/t64c/evemGGm9G0no0+dOj50uLMl5xEcDWpF3vPyWjtUeUU1tHA9vz0uTfijU0CtD+IQp6vb3rSX73xcRb11muqXHBkJJ5OZro+Y6xIZHqeARk65dv36DLdsfT9B1+zA1w3Sm5JZYOnHItNS1f6vtUoXlVumZjJt5Pp3haBACeEGSMwjK78fPryI93fVHOw0oKayuw0CFTUI4A92bPW6rW+n7TnfkV0KYl6ixLoF7RrUxfwYgXohCgngCG8ypd1gFDlTNXGPFZQ4+dasapbBN3RqN3yllZwm51hk28G8CqhrN2XeEMjhZMY3h3ql4+AzdzE4CZF/ztFGEHwDluiefV/WySwEoJ7cAtDERMkPsZWUxQN/gYUyItE0g/oLE6sVaZG9e0T4bik9swyhUk0IiAAX2fgOh8kWoSYhEtgUEZA6297w6z4FHTSeEnF34RS6oJyNxnSnkh3Hf+zv22Ng7xCoYZGGJbhKKjiZpxty6PvlVKZ7ldQiDa/XOBRjYr89TO/g+8O0kNCkqGGl3OdHFzT9PQcol72nVKp7qmD82xLHX3/Tar4i2DsemTl+sCvQeNoH5LT9g8yVtvq20TC976TUWs81vioLxSI4sKC9kr2S2gYx0SkGvzNtasrEwOtlFPj9lbxUqVmNj8BYZ4S4SpQWzb4xQ2bYkBWVVvCtEA5JUq+9enyhPUOQE4xNqhgxzrz0ybjicoDE/g4l45Kz2nASSr/wupE/x9sq2xxLqf+A2GBUyR9ZHesAAAAAElFTkSuQmCC",
                // "iVBORw0KGgoAAAANSUhEUgAAABEAAAAQCAYAAADwMZRfAAAAkUlEQVR4AWP4f9u9H4j/k40feguCDfHcJvufcTkP0fjIeUtMQ+ACt5AxTAwvxjRk0czC/w0NDXD86FjI/wv+zDjxl13meFxC2DW4vWPvvuo/o/hJDLx2TiE2V2I3BOEiojBeQxC2YmJ016AaQh7GHjuEbMZqyCBNsZiYhNiBhwVmGIDTDoPYKXQMTjtAvYMsdgCFses+xPm5ggAAAABJRU5ErkJggg==",
              },
            });
          } else if (p.name == "binaryInputStates") {
            // binaryInputStates
            const message = {
              dSUID: device.dSUID,
              value: 0,
              messageId: decodedMessage.messageId,
            };
            this.emitObject("binaryInputStateRequest", message);
            sendIt = false;
          } else if (p.name == "sensorStates") {
            // sensorStates
            const message = {
              dSUID: device.dSUID,
              value: 0,
              messageId: decodedMessage.messageId,
            };
            this.emitObject("sensorStatesRequest", message);
            sendIt = false;
          } else if (p.name == "primaryGroup") {
            // primaryGroup
            properties.push({
              name: "primaryGroup",
              value: { vUint64: device.primaryGroup },
            });
          } else if (p.name == "name") {
            // primaryGroup
            properties.push({
              name: "name",
              value: { vString: device.name },
            });
          } else if (p.name == "vendorName") {
            // vendorName
            if (device.vendorName) {
              properties.push({
                name: "vendorName",
                value: { vString: device.vendorName },
              });
            }
          } else if (p.name == "vendorId") {
            // vendorId
            if (device.vendorId) {
              properties.push({
                name: "vendorId",
                value: { vString: device.vendorId },
              });
            }
          } else if (p.name == "configURL") {
            // configURL
            if (device.configURL) {
              properties.push({
                name: "configURL",
                value: { vString: device.configURL },
              });
            }
          } else if (p.name == "modelFeatures") {
            // modelFeatures
            if (
              device.modelFeatures &&
              typeof device.modelFeatures === "object" &&
              !Array.isArray(device.modelFeatures) &&
              device.modelFeatures !== null
            ) {
              const subElements = createSubElements(device.modelFeatures);
              properties.push({
                name: p.name,
                elements: subElements,
              });
            } else {
              properties.push({
                name: p.name,
              });
            }
          } else if (p.name == "displayId") {
            properties.push({
              name: "displayId",
              value: { vString: device.displayId },
            });
          } else if (p.name == "model") {
            properties.push({
              name: p.name,
              value: { vString: device.model },
            });
          } else if (p.name == "modelUID") {
            properties.push({
              name: p.name,
              value: { vString: device.modelUID },
            });
          } else if (p.name == "modelVersion") {
            properties.push({
              name: p.name,
              value: { vString: device.modelVersion },
            });
          } else if (p.name == "name") {
            properties.push({
              name: p.name,
              value: { vString: device.name },
            });
          } else if (p.name == "channelStates") {
            // loop all indexes
            const messageNames = [];
            p.elements.forEach((el) => {
              if (device.channelDescription[0][el.name]) {
                // channel described -> emit query to get state
                messageNames.push(el.name);
              }
            });
            const message = {
              dSUID: device.dSUID,
              value: 0,
              names: messageNames,
              messageId: decodedMessage.messageId,
            };
            this.emitObject("channelStatesRequest", message);
            sendIt = false;
          }
        });
      } else {
        // device not found
        if (this.debug) {
          console.error(
            `Device ${decodedMessage.vdsmRequestGetProperty.dSUID.toLowerCase()} not found in devicelist`
          );
          if (this.debug)
            console.log(
              `Device ${decodedMessage.vdsmRequestGetProperty.dSUID.toLowerCase()} not found in devicelist`
            );
          // send not found to DS
          sendIt = false;
          const errorObj = {
            code: "ERR_NOT_FOUND",
            description: "unknown target (missing/invalid dSUID or itemSpec)",
          };
          console.log(errorObj);
          this._genericResponse(conn, errorObj, decodedMessage.messageId);
        }
      }
    }

    if (sendIt) {
      console.log(
        JSON.stringify({
          type: 5,
          messageId: decodedMessage.messageId,
          vdcResponseGetProperty: { properties },
        })
      );
      const answerObj = vdsm.fromObject({
        type: 5,
        messageId: decodedMessage.messageId,
        vdcResponseGetProperty: { properties },
      });
      const answerBuf = vdsm.encode(answerObj).finish();
      if (this.debug) console.log(JSON.stringify(vdsm.decode(answerBuf)));
      conn.write(this._addHeaders(answerBuf));
      /**
       * @event messageSent - New message sent to VDSM
       * @type {object}
       * @property Message Object
       */
      this.emitObject("messageSent", vdsm.decode(answerBuf));
    }
  }

  /**
   * Sends a generic response
   * @param  {Object} conn
   * @param  {Object} GenericResponse
   */
  _genericResponse(conn, GenericResponse, messageId) {
    // this.messageId = this.messageId + 1;
    console.log(GenericResponse);
    const answerObj = vdsm.fromObject({
      type: 1,
      messageId: messageId,
      genericResponse: { GenericResponse },
    });
    const answerBuf = vdsm.encode(answerObj).finish();
    if (this.debug) console.log(JSON.stringify(vdsm.decode(answerBuf)));
    conn.write(this._addHeaders(answerBuf));
    /**
     * @event messageSent - New message sent to VDSM
     * @type {object}
     * @property Message Object
     */
    this.emit("messageSent", vdsm.decode(answerBuf));
  }

  /**
   * Announces a new device which is then queried by the VDSM
   * @param {Object} conn
   * @param {String} dSUID - dSUID of the device which needs to be announced
   */
  _vdcSendAnnounceDevice(conn, dSUID) {
    this.messageId = this.messageId + 1;
    const answerObj = vdsm.fromObject({
      type: 10,
      messageId: this.messageId,
      vdcSendAnnounceDevice: {
        dSUID: dSUID,
        vdcDSUID: this.config.vdcDSUID,
      },
    });
    const answerBuf = vdsm.encode(answerObj).finish();
    if (this.debug) console.log(JSON.stringify(vdsm.decode(answerBuf)));
    conn.write(this._addHeaders(answerBuf));
    /**
     * @event messageSent - New message sent to VDSM
     * @type {object}
     * @property Message Object
     */
    this.emitObject("messageSent", vdsm.decode(answerBuf));
  }

  /**
   * Push a new value to the VDSM
   * @param {Object} conn
   * @param {*} obj
   */
  _vdcSendPushProperty(conn, message) {
    this.messageId = this.messageId + 1;
    const answerObj = vdsm.fromObject({
      type: 12,
      messageId: this.messageId,
      vdcSendPushProperty: { dSUID: message.dSUID, properties: message.obj },
    });
    const answerBuf = vdsm.encode(answerObj).finish();
    if (this.debug) console.log(JSON.stringify(vdsm.decode(answerBuf)));
    conn.write(this._addHeaders(answerBuf));
    /**
     * @event messageSent - New message sent to VDSM
     * @type {object}
     * @property Message Object
     */
    this.emit("messageSent", vdsm.decode(answerBuf));
  }

  /**
   * Push a new channelState
   *
   * @param {*} conn
   * @param {*} message
   */
  _vdcPushChannelStates(conn, message) {
    conn.write(this._addHeaders(message));
    this.emit("messageSent", vdsm.decode(message));
  }

  /**
   * Parse genericResponse to show errors
   * @param {Object} decodedMessage
   */
  _parseGenericResponse(decodedMessage) {
    const errorMapping = {
      ERR_OK: "Everything alright",
      ERR_MESSAGE_UNKNOWN:
        "The message id is unknown. This might happen due to incomplete or incompatible implementation",
      ERR_INCOMPATIBLE_API:
        "The API version of the VDSM is not compatible with this VDC.",
      ERR_SERVICE_NOT_AVAILABLE:
        "The VDC cannot respond. Might happen bcause the VDC is already connected to another VDSM.",
      ERR_INSUFFICIENT_STORAGE: "The VDC could not store the related data.",
      ERR_FORBIDDEN: "ERR_FORBIDDEN 5 The call is not allowed.",
      ERR_NOT_IMPLEMENTED: "Not (yet) implemented.",
      ERR_NO_CONTENT_FOR_ARRAY: "Array data was expected.",
      ERR_INVALID_VALUE_TYPE: "Invalid data type",
      ERR_MISSING_SUBMESSAGE: "Submessge was expected.",
      ERR_MISSING_DATA: "Additional data was exptected.",
      ERR_NOT_FOUND: "Addredded entity or object was not found.",
      ERR_NOT_AUTHORIZED: "The caller is not authorized with the Native Device",
    };
    if (
      decodedMessage &&
      decodedMessage.genericResponse &&
      decodedMessage.genericResponse.code
    ) {
      if (this.debug)
        console.info(
          `Generic response with message ${
            errorMapping[decodedMessage.genericResponse.code]
          } received`
        );
    }
  }

  _initDNSSD() {
    const serviceType = new dnssd.ServiceType("_ds-vdc._tcp");
    if (this.debug) console.log(serviceType);
    const ad = new dnssd.Advertisement(serviceType, this.config.port, {
      name: this.config.vdcName,
      txt: { dSUID: this.config.vdcDSUID },
    });
    ad.start()
      .on("error", function (err) {
        if (this.debug) console.log("Error:", err);
      })
      .on("stopped", function (stop) {
        if (this.debug) console.log("Stopped", stop);
      })
      .on("instanceRenamed", function (instanceRenamed) {
        if (this.debug) console.log("instanceRenamed", instanceRenamed);
      })
      .on("hostRenamed", function (hostRenamed) {
        if (this.debug) console.log("hostRenamed", hostRenamed);
      });
  }

  /**
   * Start a new instance of a VDC
   * @param {Object} config
   * @param {Array} devices
   */
  start(config, devices) {
    if (!config.vdcDSUID || config.vdcDSUID.length != 34) {
      // no vdcDSUID set -> exiting
      return { error: 99, text: "vdcDSUID missing" };
    } else {
      this.config.vdcDSUID = config.vdcDSUID;
    }
    if (!config.vdcName || config.vdcName.length <= 0) {
      // no name set -> existing
      return { error: 99, text: "vdcName missing" };
    } else {
      this.config.vdcName = config.vdcName;
    }
    if (!config.port) {
      this.config.port = DSUTIL.getFreePort();
      if (this.debug)
        console.log("No port provided -> using %d", this.config.port);
    } else {
      this.config.port = config.port;
    }
    if (config.configURL) {
      this.config.configURL = config.configURL;
    }
    this.devices = devices;

    const self = this;

    function handleConnection(conn) {
      var remoteAddress = conn.remoteAddress + ":" + conn.remotePort;
      if (self.debug)
        console.log("new client connection from %s", remoteAddress);
      conn.on("data", onConnData);
      conn.once("close", onConnClose);
      conn.on("error", onConnError);

      self.on("vdcSendPushProperty", function (obj) {
        self._vdcSendPushProperty(conn, obj);
      });

      self.on("vdcPushChannelStates", function (answerBuf) {
        self._vdcPushChannelStates(conn, answerBuf);
      });

      self.on("vdcAnnounceDevices", function () {
        if (self.devices && self.devices.length > 0) {
          self.devices.forEach((dev) => {
            if (dev.dSUID && dev.dSUID.length > 0) {
              self._vdcSendAnnounceDevice(conn, dev.dSUID);
            }
          });
        }
      });
      function onConnData(d) {
        if (self.debug)
          console.log(
            "\n---------------------------\nconnection data from %s: %j",
            remoteAddress,
            d
          );

        try {
          const decodedMessage = vdsm.decode(d.slice(2));
          /**
           * @event messageReceived - New message received from VDSM
           * @type {Object}
           * @property Message Object
           */
          self.emitObject("messageReceived", decodedMessage);
          if (self.debug) console.log(JSON.stringify(decodedMessage));
          let answerBuf = null;
          if (decodedMessage.type == 2) {
            // VDSM_REQUEST_HELLO
            // send RESPONSE HELLO
            self._vdsmResponseHello(conn, decodedMessage);
            // send vdcAnnounceVdc
            self._vdcSendAnnounceVdc(conn);
            // send announcedevice for each device
            /* if (self.devices && self.devices.length > 0) {
              self.devices.forEach((dev) => {
                if (dev.dSUID && dev.dSUID.length > 0) {
                  self._vdcSendAnnounceDevice(conn, dev.dSUID);
                }
              });
            } */
          } else if (decodedMessage.type == 8) {
            // VDSM_SEND_PING
            // send VDC_SEND_PONG
            self._vdcSendPong(conn, decodedMessage);
          } else if (decodedMessage.type == 4) {
            // VDSM_REQUESTGETPROPERTIES
            // send _vdcResponseGetProperty
            self._vdcResponseGetProperty(conn, decodedMessage);
          } else if (decodedMessage.type == 6) {
            // VDSM_SETPROPERTY
            // TODO implement logic
            // search for device and store the zone there
            const device = self.devices.find(
              (d) =>
                d.dSUID.toLowerCase() ==
                decodedMessage.vdsmRequestSetProperty.dSUID.toLowerCase()
            );
            if (device) {
              // device found
              device.zoneID =
                decodedMessage.vdsmRequestSetProperty.properties[0].value
                  .vUint64 || 65534;
              self.emitObject("deviceZoneChange", {
                request: decodedMessage.vdsmRequestSetProperty,
                devices: self.devices,
              });
            }
            self._genericResponse(
              conn,
              { code: 0, description: "OK" },
              decodedMessage.messageId
            );
          } else if (decodedMessage.type == 1) {
            self._parseGenericResponse(decodedMessage);
          } else if (decodedMessage.type == 15) {
            // VDSM_NOTIFICATION_CALL_SCENE = 15;
            if (decodedMessage.vdsmSendCallScene) {
              self.emitObject(
                "VDSM_NOTIFICATION_CALL_SCENE",
                decodedMessage.vdsmSendCallScene
              );
            }
          } else if (decodedMessage.type == 16) {
            // VDSM_NOTIFICATION_SAVE_SCENE = 16;
            if (decodedMessage.vdsmSendSaveScene) {
              self.emitObject(
                "VDSM_NOTIFICATION_SAVE_SCENE",
                decodedMessage.vdsmSendSaveScene
              );
            }
          } else if (decodedMessage.type == 17) {
            // VDSM_NOTIFICATION_UNDO_SCENE = 17;
          } else if (decodedMessage.type == 21) {
            // VDSM_NOTIFICATION_SET_CONTROL_VALUE
            if (decodedMessage.vdsmSendSetControlValue) {
              self.emitObject(
                "VDSM_NOTIFICATION_SET_CONTROL_VALUE",
                decodedMessage.vdsmSendSetControlValue
              );
            }
          } else if (decodedMessage.type == 25) {
            // VDSM_NOTIFICATION_SET_OUTPUT_CHANNEL_VALUE
            if (decodedMessage.vdsmSendOutputChannelValue) {
              self.emitObject(
                "VDSM_NOTIFICATION_SET_OUTPUT_CHANNEL_VALUE",
                decodedMessage.vdsmSendOutputChannelValue
              );
            }
          }
        } catch (err) {
          console.error(err);
        }
      }
      function onConnClose() {
        if (self.debug) console.log("connection from %s closed", remoteAddress);
      }
      function onConnError(err) {
        if (self.debug)
          console.log("Connection %s error: %s", remoteAddress, err.message);
      }
    }

    const server = net.createServer();
    server.on("connection", handleConnection);
    server.listen({ port: this.config.port }, function () {
      if (this.debug) console.log("server listening to %j", server.address());
    });
    // advertise server
    this._initDNSSD();
    setImmediate(function () {
      /**
       * @event vdcRunningState - Sent when the VDC is fully initialized
       */
      self.emitObject("vdcRunningState", { running: true });
    });
    console.log(`VDC initialized on port ${this.config.port} and running`);
  }

  sendUpdate(dSUID, obj) {
    this.emitObject("vdcSendPushProperty", { dSUID, obj });
  }

  sendState(stateValue, messageId) {
    const properties = [];
    const subElements = createSubElements({
      0: { age: 1, value: stateValue },
    });
    properties.push({
      name: "channelStates",
      elements: subElements,
    });

    console.log(
      JSON.stringify({
        type: 5,
        messageId: messageId,
        vdcResponseGetProperty: { properties },
      })
    );
    const answerObj = vdsm.fromObject({
      type: 5,
      messageId: messageId,
      vdcResponseGetProperty: { properties },
    });
    const answerBuf = vdsm.encode(answerObj).finish();
    if (this.debug) console.log(JSON.stringify(vdsm.decode(answerBuf)));
    // conn.write(this._addHeaders(answerBuf));
    /**
     * @event messageSent - New message sent to VDSM
     * @type {object}
     * @property Message Object
     */
    this.emitObject("vdcPushChannelStates", answerBuf);
    this.emitObject("messageSent", vdsm.decode(answerBuf));
  }

  sendComplexState(messageId, rawSubElements) {
    const properties = [];
    /* const subElements = createSubElements({
      name: rawSubElements.name,
      elements: rawSubElements.elements,
    }); */
    if (rawSubElements instanceof Array) {
      properties.push({
        name: "channelStates",
        elements: rawSubElements,
      });
    } else {
      properties.push({
        name: "channelStates",
        elements: [rawSubElements],
      });
    }

    console.log(
      JSON.stringify({
        type: 5,
        messageId: messageId,
        vdcResponseGetProperty: { properties },
      })
    );
    const answerObj = vdsm.fromObject({
      type: 5,
      messageId: messageId,
      vdcResponseGetProperty: { properties },
    });
    const answerBuf = vdsm.encode(answerObj).finish();
    if (this.debug) console.log(JSON.stringify(vdsm.decode(answerBuf)));
    // conn.write(this._addHeaders(answerBuf));
    /**
     * @event messageSent - New message sent to VDSM
     * @type {object}
     * @property Message Object
     */
    this.emitObject("vdcPushChannelStates", answerBuf);
    this.emitObject("messageSent", vdsm.decode(answerBuf));
  }

  sendSensorStatesRequest(sensorStates, messageId) {
    const properties = [];
    const elements = [];
    if (sensorStates && sensorStates.length > 0) {
      sensorStates.forEach((i) => {
        const subElements = createSubElements({
          age: i.age,
          error: 0,
          value_boolean: i.value,
          extendedValue: null,
        });
        elements.push({
          name: i.name,
          elements: subElements,
        });
      });

      properties.push({
        name: "channelStates",
      });
      properties.push({
        name: "sensorStates",
        elements: elements,
      });

      console.log(
        JSON.stringify({
          type: 5,
          messageId: messageId,
          vdcResponseGetProperty: { properties },
        })
      );
      const answerObj = vdsm.fromObject({
        type: 5,
        messageId: messageId,
        vdcResponseGetProperty: { properties },
      });
      const answerBuf = vdsm.encode(answerObj).finish();
      if (this.debug) console.log(JSON.stringify(vdsm.decode(answerBuf)));
      // conn.write(this._addHeaders(answerBuf));
      /**
       * @event messageSent - New message sent to VDSM
       * @type {object}
       * @property Message Object
       */
      this.emitObject("vdcPushChannelStates", answerBuf);
      this.emitObject("messageSent", vdsm.decode(answerBuf));
    }
  }

  sendBinaryInputState(inputStates, messageId) {
    const properties = [];
    const elements = [];
    if (inputStates && inputStates.length > 0) {
      inputStates.forEach((i) => {
        const subElements = createSubElements({
          age: i.age,
          error: 0,
          value_boolean: i.value,
          extendedValue: null,
        });
        elements.push({
          name: i.name,
          elements: subElements,
        });
      });
      properties.push({
        name: "binaryInputStates",
        elements: elements,
      });

      console.log(
        JSON.stringify({
          type: 5,
          messageId: messageId,
          vdcResponseGetProperty: { properties },
        })
      );
      const answerObj = vdsm.fromObject({
        type: 5,
        messageId: messageId,
        vdcResponseGetProperty: { properties },
      });
      const answerBuf = vdsm.encode(answerObj).finish();
      if (this.debug) console.log(JSON.stringify(vdsm.decode(answerBuf)));
      // conn.write(this._addHeaders(answerBuf));
      /**
       * @event messageSent - New message sent to VDSM
       * @type {object}
       * @property Message Object
       */
      this.emitObject("vdcPushChannelStates", answerBuf);
      this.emitObject("messageSent", vdsm.decode(answerBuf));
    }
  }

  sendVanish(dSUID) {
    const properties = [];
    this.messageId = this.messageId + 1;
    properties.push({
      dSUID: dSUID,
    });
    const answerObj = vdsm.fromObject({
      type: 11,
      messageId: this.messageId,
      vdcSendVanish: { properties },
    });
    const answerBuf = vdsm.encode(answerObj).finish();
    if (this.debug) console.log(JSON.stringify(vdsm.decode(answerBuf)));
    // conn.write(this._addHeaders(answerBuf));
    /**
     * @event messageSent - New message sent to VDSM
     * @type {object}
     * @property Message Object
     */
    this.emitObject("vdcPushChannelStates", answerBuf);
    this.emitObject("messageSent", vdsm.decode(answerBuf));
  }
};

module.exports = libdsvdc;
