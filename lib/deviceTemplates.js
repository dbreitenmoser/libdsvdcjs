/* const parse = require("json-templates");

const binaryInputDescription = parse({
  name: "{{genericName}}",
  elements: [
    {
      name: "aliveSignInterval",
      value: {
        vDouble: "{{aliveSignInterval}}",
      },
    },
    {
      name: "dsIndex",
      value: {
        vUint64: "{{dsIndex}}",
      },
    },
    {
      name: "inputType",
      value: {},
    },
    {
      name: "inputUsage",
      value: {
        vUint64: "{{inputUsage}}",
      },
    },
    {
      name: "maxPushInterval",
      value: {
        vDouble: "{{maxPushInterval}}",
      },
    },
    {
      name: "name",
      value: {
        vString: "{{name}}",
      },
    },
    {
      name: "sensorFunction",
      value: {
        vUint64: "{{sensorFunction}}",
      },
    },
    {
      name: "type",
      value: {
        vString: "binaryInput",
      },
    },
    {
      name: "updateInterval",
      value: {
        vDouble: "{{updateInterval}}",
      },
    },
    {
      name: "x-p44-behaviourType",
      value: {
        vString: "binaryInput",
      },
    },
  ],
});

const outputDescription = parse({[
    {
      name: "dsIndex",
      value: {
        vUint64: "{{dsIndex}}",
      },
    },
    {
      name: "name",
      value: {
        vString: "{{name}}",
      },
    },
    {
      name: "function",
      value: {
        vUint64: "{{function}}",
      },
    },
    {
      name: "outputUsage",
      value: {
        vUint64: "{{outputUsage}}",
      },
    },
    {
      name: "type",
      value: {
        vString: "{{type}}",
      },
    },
    {
      "name":"variableRamp",
      "value":{
        "vBool":"{{variableRamp}}"
      }
    }, 
]})

const outputSetting = parse({[
         {
            "name":"groups",
            "elements":[
               {
                  "name":"0",
                  "value":{
                     "vBool":true
                  }
               },
               {
                  "name":"1",
                  "value":{
                     "vBool":true
                  }
               }
            ]
         },
         {
            "name":"mode",
            "value":{
               "vUint64":"{{mode}}"
            }
         },
         {
            "name":"pushChanges",
            "value":{
               "vBool":"{{pushChanges}}"
            }
         },
]})

const sensorDescription = parse({
  name: "{{sensorName}}",
  elements: [
    {
      name: "aliveSignInterval",
      value: {
        vDouble: "{{aliveSignInterval}}",
      },
    },
    {
      name: "dsIndex",
      value: {
        vUint64: "{{dsIndex}}",
      },
    },
    {
      name: "max",
      value: {
        vDouble: "{{max}}",
      },
    },
    {
      name: "maxPushInterval",
      value: {
        vDouble: "{{maxPushInterval}}",
      },
    },
    {
      name: "min",
      value: {
        vDouble: "{{min}}",
      },
    },
    {
      name: "name",
      value: {
        vString: "{{name}}",
      },
    },
    {
      name: "resolution",
      value: {
        vDouble: "{{resolution}}",
      },
    },
    {
      name: "sensorType",
      value: {
        vUint64: "{{sensorType}}",
      },
    },
    {
      name: "sensorUsage",
      value: {
        vUint64: "{{sensorUsage}}",
      },
    },
    {
      name: "siunit",
      value: {
        vString: "{{siunit}}",
      },
    },
    {
      name: "symbol",
      value: {
        vString: "{{symbol}}",
      },
    },
    {
      name: "type",
      value: {
        vString: "sensor",
      },
    },
    {
      name: "updateInterval",
      value: {
        vDouble: "{{updateInterval}}",
      },
    },
    {
      name: "x-p44-behaviourType",
      value: {
        vString: "sensor",
      },
    },
  ],
});

module.exports = {
  binaryInputDescription,
  sensorDescription,
  outputDescription,
  outputSetting
};*/
