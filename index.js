const libdsvdc = require("./lib/libdsvdc");
const libdsutil = require("./lib/libdsutil");

module.exports = {
  libdsvdc: libdsvdc,
  libdsutil: libdsutil,
};
